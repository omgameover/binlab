var webpack = require('webpack');
var path = require('path');
var projectRoot = path.resolve(__dirname, '/')
var HtmlWebpackPlugin = require('html-webpack-plugin');
var LiveReloadPlugin = require('webpack-livereload-plugin');
var src = path.join(__dirname, 'src');

var config = {
  devServer: {
    hot: true,
    inline: true,
    port: 8080,
  },
  stats: {
    assets: false,
    colors: true,
    version: false,
    hash: true,
    timings: true,
    chunks: false,
    chunkModules: false
  },
  entry: {
    styles: path.join(src, 'styles.sass'),
    index: path.join(src, 'index.pug'),
    bundle: path.join(src, 'index.js'),
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.pug$/,
        use:  ['html-loader', 'pug-html-loader?pretty&exports=false']
      },
      {
        test: /\.sass$/,
        use: ['style-loader', 'css-loader', 'resolve-url-loader', 'sass-loader?sourceMap'],
      },
      {
        test: /\.jpe?g$|\.gif$|\.svg$|\.png$/i,
        loader: "file-loader?name=/img/[name].[ext]"
      },
      {
        test: /\.(eot|ttf|woff|woff2|otf)$/,
        loader: 'file-loader?name=/fonts/[name].[ext]'
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'index.html',
      template: path.join(src, 'index.pug'),
    }),
    // new HtmlWebpackPlugin({
    //   title: 'ru.html',
    //   template: path.join(src, 'ru.pug'),
    //   filename: 'ru/index.html'
    // }),
    // new HtmlWebpackPlugin({
    //   title: 'en.html',
    //   template: path.join(src, 'en.pug'),
    //   filename: 'en/index.html'
    // }),
    new LiveReloadPlugin()
  ],
  resolve: {
    extensions: ['.webpack.js', '.web.js', '.js']
  },
  target: 'web'
};

module.exports = config;
