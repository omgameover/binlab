/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 68);
/******/ })
/************************************************************************/
/******/ ({

/***/ 68:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;var $el, leftPos, newWidth, $mainNav = $('.menu');
var $menuLine = $('#menu-line');

function setMenuLine() {
  $el = $('.active-menu-item');
  leftPos = $el.position().left;
  // newWidth = parseInt($el.width(), 10) - 10 + 'px';
  // if (navigator.userAgent.toLowerCase().indexOf('firefox') !== -1 || window.chrome) {
  //   newWidth = parseInt($el.width(), 10) + 30 + 'px';
  // }
  // newWidth = parseInt($el.css('width'), 10) - 50 + 'px';
  newWidth = ($el.innerWidth() - 10) + 'px'
  $menuLine.stop().animate({ left: leftPos, width: newWidth });
}

$('.description').mCustomScrollbar({
  scrollButtons: { enable: true },
  axis: 'x',
  mouseWheel: {
    enable: true,
    axis: 'x',
    invert: true,
    preventDefault: false
  }
});

$('#projects .content').mCustomScrollbar({
  scrollButtons: { enable: true },
  axis: 'x',
  mouseWheel: {
    axis: 'x',
    invert: true
  },
  alwaysShowScrollbar: 2
});

$('.hamburger').click(function() {
  $('.site-container').addClass('blur');
  $('.menu').addClass('transparent');
  $('.hamburger').addClass('transparent');
  $('body').addClass('noscroll');
  setTimeout(function() { $('.menu').addClass('fullscreen') }, 50);
  setTimeout(function() { $('.fullscreen').addClass('on') }, 150);
});

$('.close').click(function() {
  $('.site-container').removeClass('blur');
  $('.fullscreen').removeClass('transparent');
  $('body').removeClass('noscroll');
  $('.hamburger').removeClass('transparent');
  setTimeout(function() { $('.fullscreen').removeClass('on') }, 50);
  setTimeout(function() { $('.menu').removeClass('fullscreen') }, 500);

  setTimeout(function() {
    setMenuLine();
  }, 550);
});

$('#send-feedback').click(function() {
  $('.feedback-container').removeClass('on');
  $('.menu').removeClass('blur');
  $('body').removeClass('noscroll');
  $('.site-container').removeClass('blur');
});

function detectIE() {
  var ua = window.navigator.userAgent;

  var msie = ua.indexOf('MSIE ');
  if (msie > 0) {
    // IE 10 or older => return version number
    return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
  }

  var trident = ua.indexOf('Trident/');
  if (trident > 0) {
    // IE 11 => return version number
    var rv = ua.indexOf('rv:');
    return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
  }
  var edge = ua.indexOf('Edge/');
  if (edge > 0) {
    // Edge (IE 12+) => return version number
    return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
  }

  // other browser
  return false;
}

$('button.btn.feedback').click(function() {
  setTimeout(function() {
    $('.feedback-container').addClass('on');
    $('.menu').addClass('blur');
    $('body').addClass('noscroll');
    if (navigator.userAgent.toLowerCase().indexOf('firefox') == -1 && !detectIE()) {
      $('.site-container').addClass('blur');
    }
   }, 200);
});

$('#send-order').click(function() {
  $('.order-container').removeClass('on');
  $('.menu').removeClass('blur');
  $('body').removeClass('noscroll');
  $('.site-container').removeClass('blur');
});

$('button.btn.order').click(function() {
  setTimeout(function() {
    $('.order-container').addClass('on');
    $('.menu').addClass('blur');
    $('body').addClass('noscroll');
    if (navigator.userAgent.toLowerCase().indexOf('firefox') == -1 && !detectIE()) {
      $('.site-container').addClass('blur');
    }
   }, 200);
});

$('.close-feedback-order').click(function() {
  $('.feedback-container').removeClass('on');
  $('.order-container').removeClass('on');
  $('.menu').removeClass('blur');
  $('body').removeClass('noscroll');
  $('.site-container').removeClass('blur');
});

function parallax() {
  $(window).scroll(function() {
    var st = $(this).scrollTop();

    if (st >= 50) {
      $('.buttons').css('bottom', '5%');
    } else {
      $('.buttons').css('bottom', '');
    }

    $('.parallax1').css('transform', 'translateY(' + st * 4 + '%)');
    $('.parallax2').css('transform', 'translateY(' + st + '%)');
    $('.parallax3').css('transform', 'translateY(' + st * 2 + '%)');
    $('.parallax4').css('transform', 'translateY(' + st / 2 + '%)');
    $('.parallax6').css('transform', 'translateY(' + st / 1.8 + '%)');
    $('.parallax7').css('transform', 'translateY(' + (st - 1200) / 1.5 + '%)');
    $('.parallax9').css('transform', 'translateY(' + (st - 600) * 1.5 + '%)');
    $('.parallax10').css('transform', 'translateY(' + (st - 2800) / 4 + '%)');
    $('.parallax11').css('transform', 'translateY(' + (st - 2700) / 6 + '%)');
    $('.parallax13').css('transform', 'translateY(' + (st - 4000) * 2 + '%)');
    $('.parallax14').css('transform', 'translateY(' + (st - 4000) / 2 + '%)');
    $('.parallax16').css('transform', 'translateY(' + (st - 3500) / 1.7 + '%)');

    var scrollTop = $(window).scrollTop(),
      elementOffset = $('.parallax7').offset().top,
      distance = (elementOffset - scrollTop + 13);
      height = $(window).innerHeight()
      percent = distance / height
      scissors = (532 * percent * (-1)) + 532
    $('.parallax7-scissors').css('transform', 'translateX(' + scissors + 'px)');
  });
}

if (window.innerWidth > 900) {
  parallax();
}

$(window).resize(function() {
  if (window.innerWidth > 900) {
    parallax();
  } else {
    $(window).unbind('scroll');
    $('.parallax1').css('transform', 'translateY(0%)');
    $('.parallax2').css('transform', 'translateY(0%)');
    $('.parallax3').css('transform', 'translateY(0%)');
    $('.parallax4').css('transform', 'translateY(0%)');
    $('.parallax6').css('transform', 'translateY(0%)');
    $('.parallax7').css('transform', 'translateY(0%)');
    $('.parallax9').css('transform', 'translateY(0%)');
    $('.parallax10').css('transform', 'translateY(0%)');
    $('.parallax11').css('transform', 'translateY(0%)');
    $('.parallax13').css('transform', 'translateY(0%)');
    $('.parallax14').css('transform', 'translateY(0%)');
    $('.parallax16').css('transform', 'translateY(0%)');
    $('.buttons').css('bottom', '');
  }

  setMenuLine();
});

$('.menu-item').click(function() {
  $('.menu-item').removeClass('active-menu-item');
  $(this).addClass('active-menu-item');
  $('.site-container').removeClass('blur');
  $('.fullscreen').removeClass('transparent');
  $('.hamburger').removeClass('transparent');
  $('.fullscreen').removeClass('on');
  $('.menu').removeClass('fullscreen');

  setMenuLine();

  // $el = $(this);
  // leftPos = $el.position().left;
  // newWidth = $el.width();
  // $menuLine.stop().animate({ left: leftPos, width: newWidth });

  $('.menu-list').toggleClass('active');

  var addScroll = 0;

  if (window.innerWidth > 900) {
    addScroll = 50
  }

  $("html, body").animate({
    scrollTop: $($(this).attr("href")).offset().top + addScroll + "px"
  }, {
    duration: 2500,
    easing: "easeInOutCubic"
  });
  return false;
});

$(window).on('load', function() {
  $('.feedback-container').removeClass('no');
  $('.order-container').removeClass('no');
  setMenuLine()
});

function classReg( className ) {
  return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
}

var hasClass, addClass, removeClass;

if ( 'classList' in document.documentElement ) {
  hasClass = function( elem, c ) {
    return elem.classList.contains( c );
  };
  addClass = function( elem, c ) {
    elem.classList.add( c );
  };
  removeClass = function( elem, c ) {
    elem.classList.remove( c );
  };
}
else {
  hasClass = function( elem, c ) {
    return classReg( c ).test( elem.className );
  };
  addClass = function( elem, c ) {
    if ( !hasClass( elem, c ) ) {
      elem.className = elem.className + ' ' + c;
    }
  };
  removeClass = function( elem, c ) {
    elem.className = elem.className.replace( classReg( c ), ' ' );
  };
}

function toggleClass( elem, c ) {
  var fn = hasClass( elem, c ) ? removeClass : addClass;
  fn( elem, c );
}

var classie = {
  hasClass: hasClass,
  addClass: addClass,
  removeClass: removeClass,
  toggleClass: toggleClass,
  has: hasClass,
  add: addClass,
  remove: removeClass,
  toggle: toggleClass
};

if ( true ) {
  !(__WEBPACK_AMD_DEFINE_FACTORY__ = (classie),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
} else {
  window.classie = classie;
}

var buttons9Click = Array.prototype.slice.call( document.querySelectorAll( 'button.btn' ) ),
  totalButtons9Click = buttons9Click.length;

buttons9Click.forEach( function( el, i ) { el.addEventListener( 'click', activate, false ); } );

function activate() {
  var self = this, activatedClass = 'btn-activated';

  if( classie.has( this, 'btn' ) ) {
    activatedClass = 'btn-active';
  }

  if( !classie.has( this, activatedClass ) ) {
    classie.add( this, activatedClass );
    setTimeout( function() { classie.remove( self, activatedClass ) }, 1000 );
  }
}

$('.item-list').click(function() {
  $('.item-list').removeClass('active');
  $(this).addClass('active');
  $('#mCSB_1_container').css('left', '0px');
  $('#mCSB_1_dragger_horizontal').css('left', '0px');
});

$(function() {
  $('#tabs').tabs();
  $('#tabs').tabs({ active: 0 });
});


/***/ })

/******/ });